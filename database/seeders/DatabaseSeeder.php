<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\USer;
use App\Models\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name'=>'Admin',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('123456'),
            'role'=>'admin',
        ]);

        User::create([
            'name'=>'Member',
            'email'=>'member@gmail.com',
            'password'=>bcrypt('123456'),
            'role'=>'user',
        ]);

        Product::create([
            'uuid' => '25769c6cd34d4bfeba98e0ee856f3e7a',
            'name' => 'buku',
            'type' => 'atk',
            'price' => 50000,
            'quantity' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
