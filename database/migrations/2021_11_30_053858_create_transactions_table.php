<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->text('uuid');
            $table->bigInteger('user_id');
            $table->bigInteger('product_id');
            $table->integer('amount');
            $table->integer('tax');
            $table->integer('admin_fee');
            $table->integer('total');
            $table->timestamps();
            $table->dateTime('deleted_at')->nulable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
