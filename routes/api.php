<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'App\Http\Controllers\UserController@login');
Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('logout', 'App\Http\Controllers\UserController@logout');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'App\Http\Controllers\UserController@details');
});

Route::get('product','App\Http\Controllers\ProductController@index');
Route::post('product-store','App\Http\Controllers\ProductController@store');
Route::get('product/{uuid}','App\Http\Controllers\ProductController@show');
Route::post('product/edit/{uuid}','App\Http\Controllers\ProductController@update');
Route::post('product/delete/{uuid}','App\Http\Controllers\ProductController@destroy');
